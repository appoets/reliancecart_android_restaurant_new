package com.copiaexigo.grocery.restaurants.model.allergies;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sub {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("parent_id")
@Expose
private Integer parentId;
@SerializedName("name")
@Expose
private String name;
@SerializedName("status")
@Expose
private String status;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public Integer getParentId() {
return parentId;
}

public void setParentId(Integer parentId) {
this.parentId = parentId;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

}