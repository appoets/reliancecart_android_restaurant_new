package com.copiaexigo.grocery.restaurants.model.allergies;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Allergies implements Parcelable {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("allergies_id")
@Expose
private Integer allergiesId;
@SerializedName("product_id")
@Expose
private Integer productId;
@SerializedName("allergies")
@Expose
private Allergies_ allergies;

    protected Allergies(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            allergiesId = null;
        } else {
            allergiesId = in.readInt();
        }
        if (in.readByte() == 0) {
            productId = null;
        } else {
            productId = in.readInt();
        }
        if (in.readByte() == 0) {
            allergies = null;
        } else {
            allergies= in.readParcelable(Allergies.class.getClassLoader());
        }

    }

    public static final Creator<Allergies> CREATOR = new Creator<Allergies>() {
        @Override
        public Allergies createFromParcel(Parcel in) {
            return new Allergies(in);
        }

        @Override
        public Allergies[] newArray(int size) {
            return new Allergies[size];
        }
    };

    public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public Integer getAllergiesId() {
return allergiesId;
}

public void setAllergiesId(Integer allergiesId) {
this.allergiesId = allergiesId;
}

public Integer getProductId() {
return productId;
}

public void setProductId(Integer productId) {
this.productId = productId;
}

public Allergies_ getAllergies() {
return allergies;
}

public void setAllergies(Allergies_ allergies) {
this.allergies = allergies;
}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        if (allergiesId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(allergiesId);
        }
        if (productId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(productId);
        }

        if (allergies == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
           parcel.writeParcelable(allergies,i);
        }

    }
}