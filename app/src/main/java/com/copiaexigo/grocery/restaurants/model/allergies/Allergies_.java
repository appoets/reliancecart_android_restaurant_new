package com.copiaexigo.grocery.restaurants.model.allergies;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Allergies_ implements Parcelable {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("parent_id")
@Expose
private Integer parentId;
@SerializedName("name")
@Expose
private String name;
@SerializedName("status")
@Expose
private String status;

    protected Allergies_(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            parentId = null;
        } else {
            parentId = in.readInt();
        }
        name = in.readString();
        status = in.readString();
    }

    public static final Creator<Allergies_> CREATOR = new Creator<Allergies_>() {
        @Override
        public Allergies_ createFromParcel(Parcel in) {
            return new Allergies_(in);
        }

        @Override
        public Allergies_[] newArray(int size) {
            return new Allergies_[size];
        }
    };

    public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public Integer getParentId() {
return parentId;
}

public void setParentId(Integer parentId) {
this.parentId = parentId;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        if (parentId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(parentId);
        }
        parcel.writeString(name);
        parcel.writeString(status);
    }
}