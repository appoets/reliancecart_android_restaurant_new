package com.copiaexigo.grocery.restaurants.controller;

import com.copiaexigo.grocery.restaurants.model.Profile;

/**
 * Created by Tamil on 3/16/2018.
 */

public interface ProfileListener {

    void onSuccess(Profile profile);

    void onFailure(String error);
}
