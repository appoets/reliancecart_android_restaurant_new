package com.copiaexigo.grocery.restaurants.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.copiaexigo.grocery.restaurants.R;
import com.copiaexigo.grocery.restaurants.adapter.MyCategoriesExpandableListAdapter;
import com.copiaexigo.grocery.restaurants.helper.GlobalData;
import com.copiaexigo.grocery.restaurants.model.allergies.AllergiesDataItem;
import com.copiaexigo.grocery.restaurants.model.allergies.DataItem;
import com.copiaexigo.grocery.restaurants.model.allergies.Sub;
import com.copiaexigo.grocery.restaurants.model.allergies.SubCategoryItem;
import com.copiaexigo.grocery.restaurants.network.ApiClient;
import com.copiaexigo.grocery.restaurants.network.ApiInterface;
import com.copiaexigo.grocery.restaurants.plugin.ConstantManager;
import com.copiaexigo.grocery.restaurants.plugin.interfaces.AllergiesListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllergySetActivity extends AppCompatActivity {

    @Nullable
    @BindView(R.id.rv_allergies)
    ExpandableListView rvAllergies;

    @Nullable
    @BindView(R.id.button2)
    Button btnSubmit;

    private ArrayList<DataItem> arCategory;
    private ArrayList<SubCategoryItem> arSubCategory;

    private ArrayList<AllergiesDataItem> allergiesDataItems;
    private ArrayList<AllergiesDataItem> allergiesSubDataItems;

    private ArrayList<HashMap<String, String>> parentItems;
    private ArrayList<ArrayList<HashMap<String, String>>> childItems;
    private MyCategoriesExpandableListAdapter myCategoriesExpandableListAdapter;

    private static AllergiesListener allergiesListener;

    ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allergy_set);
        ButterKnife.bind(this);

       // allergiesListener.allergiesDone();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalData.allergies.clear();
                String names="";
                for (int i = 0; i < parentItems.size(); i++) {
                    String isChecked = parentItems.get(i).get(ConstantManager.Parameter.IS_CHECKED);

                    if (isChecked.equalsIgnoreCase(ConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                        GlobalData.allergies.add(Integer.parseInt(parentItems.get(i).get(ConstantManager.Parameter.CATEGORY_ID)));
                        //Toast.makeText(getApplicationContext(), parentItems.get(i).get(ConstantManager.Parameter.CATEGORY_NAME), Toast.LENGTH_SHORT).show();
                        names+=""+parentItems.get(i).get(ConstantManager.Parameter.CATEGORY_NAME)+",";
                    }

                    for (int j = 0; j < childItems.get(i).size(); j++) {

                        String isChildChecked = childItems.get(i).get(j).get(ConstantManager.Parameter.IS_CHECKED);

                        if (isChildChecked.equalsIgnoreCase(ConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                            GlobalData.allergies.add(Integer.parseInt(childItems.get(i).get(j).get(ConstantManager.Parameter.SUB_ID)));
                            names+=""+childItems.get(i).get(j).get(ConstantManager.Parameter.SUB_CATEGORY_NAME)+",";
                        }

                    }

                }
              //  allergiesListener.allergiesDone();
                Log.d("MMMID", "" + GlobalData.allergies.toString());
                Intent intent=new Intent();
                intent.putExtra("names",""+names);
                setResult(RESULT_OK,intent);
                finish();
            }
        });


        setupReferences();
    }

    private void setupReferences() {

        //  rvAllergies = findViewById(R.id.rv_allergies);

        allergiesDataItems = new ArrayList<>();
        allergiesSubDataItems = new ArrayList<>();

        arCategory = new ArrayList<>();
        arSubCategory = new ArrayList<>();

        parentItems = new ArrayList<>();
        childItems = new ArrayList<>();

        Call<ArrayList<AllergiesDataItem>> getprofile = apiInterface.getAllergies();
        getprofile.enqueue(new Callback<ArrayList<AllergiesDataItem>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<AllergiesDataItem>> call, @NonNull Response<ArrayList<AllergiesDataItem>> response) {
                if (response.isSuccessful()) {
                    allergiesDataItems = response.body();
                    for (AllergiesDataItem d : allergiesDataItems) {
                        DataItem dataItem = new DataItem();
                        dataItem.setCategoryId("" + d.getId());
                        dataItem.setCategoryName(d.getName());
                        arSubCategory = new ArrayList<>();
                        for (Sub s : d.getSub()) {
                            SubCategoryItem subCategoryItem = new SubCategoryItem();
                            subCategoryItem.setSubId(s.getId());
                            subCategoryItem.setIsChecked(ConstantManager.CHECK_BOX_CHECKED_FALSE);
                            subCategoryItem.setSubCategoryName(s.getName());
                            arSubCategory.add(subCategoryItem);
                        }
                        dataItem.setSubCategory(arSubCategory);
                        arCategory.add(dataItem);

                    }

                    for (DataItem data : arCategory) {
//                        Log.i("Item id",item.id);
                        ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
                        HashMap<String, String> mapParent = new HashMap<String, String>();

                        mapParent.put(ConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
                        mapParent.put(ConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

                        int countIsChecked = 0;
                        for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                            HashMap<String, String> mapChild = new HashMap<String, String>();
                            mapChild.put(ConstantManager.Parameter.SUB_ID, "" + subCategoryItem.getSubId());
                            mapChild.put(ConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                            mapChild.put(ConstantManager.Parameter.CATEGORY_ID, "" + subCategoryItem.getCategoryId());
                            mapChild.put(ConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                            if (subCategoryItem.getIsChecked().equalsIgnoreCase(ConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                                countIsChecked++;
                            }
                            childArrayList.add(mapChild);
                        }

                       /* if (countIsChecked == data.getSubCategory().size()) {
                            data.setIsChecked(ConstantManager.CHECK_BOX_CHECKED_TRUE);
                        } else {
                            data.setIsChecked(ConstantManager.CHECK_BOX_CHECKED_FALSE);
                        }*/

                        mapParent.put(ConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
                        childItems.add(childArrayList);
                        parentItems.add(mapParent);

                    }

                    ConstantManager.parentItems = parentItems;
                    ConstantManager.childItems = childItems;

                    myCategoriesExpandableListAdapter = new MyCategoriesExpandableListAdapter(getApplicationContext(), parentItems, childItems, false);
                    rvAllergies.setAdapter(myCategoriesExpandableListAdapter);

                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().toString());
                        Toast.makeText(getApplicationContext(), jObjError.optString("error"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<AllergiesDataItem>> call, @NonNull Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        Log.d("TAG", "setupReferences: " + arCategory.size());


    }
}